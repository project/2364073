<?php
/**
 * @file define admin menu path and implement hook_domain_source_path_alter().
 */

/**
 * Implements hook_menu().
 */
function domain_source_path_alter_menu() {
  $items = array();
  $items['admin/structure/domain/sourcepathalter'] = array(
    'title' => 'Source Path Alter',
    'access arguments' => array('administer domain source path alter'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_source_path_alter_overview'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'domain_source_path_alter.admin.inc',
    'description' => 'Manage domain source path alter.',
    'weight' => -1,
  );

  $items['admin/structure/domain/sourcepathalter/edit'] = array(
    'title' => 'Create Domain Source Path ',
    'access arguments' => array('administer domain source path alter'),
    'type' => MENU_LOCAL_ACTION,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_source_path_alter_form'),
    'file' => 'domain_source_path_alter.admin.inc',
    'description' => 'Create new domain source path entry.',
  );

  $items['admin/structure/domain/sourcepathalter/delete'] = array(
    'title' => 'Delete Domain Source Path ',
    'access arguments' => array('administer domain source path alter'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_source_path_alter_delete'),
    'file' => 'domain_source_path_alter.admin.inc',
    'description' => 'Delete domain source path entry.',
  );
  
  return $items;
}

/**
 * Implements hook_permission().
 */
function domain_source_path_alter_permission() {
  
  $permissions = array(
    'administer domain source path alter' => array(
      'title' => t('Administer domain source path alter'),
    ),
  );

  return $permissions;
}

/**
 * Implement hook_domain_source_path_alter().
 */
function domain_source_path_alter_domain_source_path_alter(&$source, $path) {
  
  // try to find the path in domain source path alter table
  if ($row = domain_source_path_alter_get_for_path($path)) {
    $source = domain_lookup($row->domain_id);
  }
}

/**
 * @return all domain source path alter entries. 
 */
function domain_source_path_alter_get_all() {
  
  // try to find the path in domain source path alter table
  return db_query('SELECT n.dspa_id, n.path, n.domain_id, d.sitename from {domain_source_path_alter} n LEFT JOIN {domain} d ON n.domain_id = d.domain_id');
}

/**
 * @param $path
 * @return a domain source path for given path, or FALSE. 
 */
function domain_source_path_alter_get_for_path($path = "") {
  
  // try to find the path in domain source path alter table
  $rows = db_query('SELECT n.path, n.dspa_id, n.domain_id FROM {domain_source_path_alter} n WHERE n.path = :path LIMIT 1', array(':path' => $path));

  // if we found the path in db, alter domain source for this domain
  if ($rows->rowCount() == 1) {
    return $rows->fetchObject();
  } 

  // otherwise return FALSE
  else {
    return FALSE;
  }
}

/**
 * @param $dspa_id
 * @return a domain source path for given id, or FALSE. 
 */
function domain_source_path_alter_get_for_dspa_id($dspa_id = -1) {
  
  // try to find the path in domain source path alter table
  $rows = db_query('SELECT n.dspa_id, n.path, n.domain_id FROM {domain_source_path_alter} n WHERE n.dspa_id = :dspa_id LIMIT 1', array(':dspa_id' => $dspa_id));

  // if we found the path in db, alter domain source for this domain
  if ($rows->rowCount() == 1) {
    return $rows->fetchObject();

  } 

  // otherwise return false
  else {
    return FALSE;
  }
}
