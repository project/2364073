<?php
/**
 * @file define the admin interface for domain source path alter. 
 */

/**
 * Create an overview form for all domain source path alter entries.
 */
function domain_source_path_alter_overview($form, &$form_state) {

  // Get all domain source path alter entries. 
  $rows = array();
  foreach (domain_source_path_alter_get_all() as $row) {
    $rows[$row->dspa_id] = array(
      'path' => $row->path, 
      'domain' => $row->sitename, 
      'edit' => l(t('Edit'), 'admin/structure/domain/sourcepathalter/edit/' . $row->dspa_id), 
      'delete' => l(t('Delete'), 'admin/structure/domain/sourcepathalter/delete/' . $row->dspa_id), 
    );
  }

  // Draw the form elements. 
  $form['top'] = array(
    '#markup' => t('<p>The following entries override the default domain sources for each node, taxonomy, view, ... at the path.</p>'), 
  );

  $form['list'] = array(
    '#theme' => 'table', 
    '#header' => array(t('Path'), t('Domain'), t('Edit'), t('Delete')), 
    '#rows' => $rows, 
  );

  return $form;
}

/**
 * Create an add/edit form for domain source path alter entries.
 */
function domain_source_path_alter_form($form, &$form_state, $dspa_id = -1) {

  // create a path text edit
  $form['path'] = array(
    '#type' => 'textfield', 
    '#title' => t('Path'), 
    '#description' => t('Set the path, you want to override the domain source.'), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );

  // create a domain select list
  $domains = array();

  foreach (domain_domains() as $key => $value) {
    $domains[$value['domain_id']] = $value['sitename'];
  }

  $form['domain_id'] = array(
    '#type' => 'select', 
    '#title' => t('Domain'), 
    '#required' => TRUE,
    '#description' => t('Select the domain, you want to set as source to the path.'), 
    '#required' => TRUE,
    '#options' => $domains,
  );

  // create submit button
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'), 
  );

  // load default values for this entry and add dspa_id as hidden field
  if (intval($dspa_id) > 0) {
    if ($entry = domain_source_path_alter_get_for_dspa_id(intval($dspa_id))) {
      $form['path']['#default_value'] = $entry->path;
      $form['path']['#disabled'] = TRUE;
      $form['domain_id']['#default_value'] = $entry->domain_id;


      $form['dspa_id'] = array(
        '#type' => 'hidden', 
        '#value' => $entry->dspa_id, 
      );
    }
  }

  return $form;
}

function domain_source_path_alter_form_validate($element, &$form_state) {
  //email validation not working how to validate email
  $path = $form_state['values']['path'];
  $domain_id = $form_state['values']['domain_id'];

  // check for valid path
  if (!drupal_valid_path($path)) {
    form_error($element['path'], t('Please enter a valid path.'));
  }

  // check if path is already in database for new entries
  if (!isset($form_state['values']['dspa_id']) && domain_source_path_alter_get_for_path($path)) {
    form_error($element['path'], t('There is already an entry for this path.'));
  }

  // @todo check for duplicat

  // check for valid domain
  $domain = domain_load($domain_id);

  if (!isset($domain['valid']) || !$domain['valid']) {
    form_error($element['domain_id'], t('Select a valid domain.'));
  }
}

function domain_source_path_alter_form_submit($form, &$form_state) {

  $path = $form_state['values']['path'];
  $domain_id = $form_state['values']['domain_id'];

  // if this is an edit submit
  if (isset($form_state['values']['dspa_id'])) {

    // execute the update query
    $result = db_update('domain_source_path_alter')
      ->fields(array(
        'domain_id' => $domain_id,
      ))
      ->condition('dspa_id', intval($form_state['values']['dspa_id']), '=')
      ->execute();

    // add status message
    if ($result > 0) {
      drupal_set_message(t('The entry was updated successfully!'));
    } 

    else {
      drupal_set_message(t('Nothing changed.'), 'warning');
    }
  } 

  // if this is an add submit
  else {

    // execute the isert query
    $result = db_insert('domain_source_path_alter')
      ->fields(array(
        'path' => $path,
        'domain_id' => $domain_id,
      ))
      ->execute();

    // add status message
    if ($result > 0) {
      drupal_set_message(t('The entry was created successfully!'));
    } 

    else {
      drupal_set_message(t('Nothing changed.'), 'warning');
    }
  }

  // redirect to domain source path alter overview
  $form_state['redirect'] = 'admin/structure/domain/sourcepathalter';
}

function domain_source_path_alter_delete($form, &$form_state, $dspa_id = -1) {

  // load default values for this entry and add dspa_id as hidden field
  if (intval($dspa_id) > 0) {
    if ($entry = domain_source_path_alter_get_for_dspa_id(intval($dspa_id))) {

      // Add hidden form field for transporting the dspa_id to the submit function.
      $form['dspa_id'] = array(
        '#type' => 'hidden', 
        '#value' => $entry->dspa_id, 
      );

      // return a confirm_form to allow the user the be surre about deleting one more time. 
      return confirm_form(
        $form, 
        $question= t("Do you really want to delete the entry for path: '" . $entry->path . "' this entry?"), 
        'admin/structure/domain/sourcepathalter', 
        $description = t("Warning, you cannot undo this!"), 
        "Delete"
      );

    }
  }
}

function domain_source_path_alter_delete_submit($form, &$form_state) {

  // If we got the dspa_id, we delete the entry. 
  if (isset($form_state['values']['dspa_id'])) {

    // execute delete query. 
    $result = db_delete('domain_source_path_alter')
      ->condition('dspa_id', intval($form_state['values']['dspa_id']))
      ->execute();

    // add status message
    if ($result > 0) {
      drupal_set_message(t('The entry was deleted successfully!'));
    } 

    else {
      drupal_set_message(t('Nothing changed.'), 'warning');
    }

  }

  $form_state['redirect'] = 'admin/structure/domain/sourcepathalter';
}
